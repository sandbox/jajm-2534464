<?php

/**
 * @file
 * Search API data alteration callback.
 */
class SearchApiAttachmentsLinkAlterSettings extends SearchApiAttachmentsAlterSettings {

  /**
   * {@inheritdoc}
   */
  public function configurationForm() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterItems(array &$items) {
    $index_fields = $this->index->getFields();
    foreach ($items as $id => &$item) {
      foreach ($this->getLinkFields() as $name => $field) {
        $attachments = 'attachments_' . $name;
        if (isset($index_fields[$attachments]) && isset($item->{$name})) {
          if (isset($item->{$name}['und'])) {
            foreach ($item->{$name}['und'] as $value) {
              $url = $value['url'];
              if (isset($item->{$attachments})) {
                $item->{$attachments} .= ' ' . $this->getUrlContent($url);
              } else {
                $item->{$attachments} = $this->getUrlContent($url);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Get text content of file at given URL.
   */
  protected function getUrlContent($url) {
    $extraction = FALSE;

    $extraction_method = variable_get('search_api_attachments_extract_using', 'tika');
    // Send the extraction request to the right place depending on the
    // current setting.
    if ($extraction_method == 'tika') {
      watchdog('Search API Attachments Field Link', 'Tika extraction method is not implemented', NULL, WATCHDOG_ERROR);
    }
    else {
      $extraction = $this->extract_solr($url);
    }

    return $extraction;
  }

  /**
   * Get file content using Solr Extraction.
   */
  protected function extract_solr($url) {
    $extraction = FALSE;

    try {
      // Server name is stored in the index.
      $server_name = $this->index->server;

      $server = search_api_server_load($server_name, TRUE);

      // Make sure this is a solr server.
      $class_info = search_api_get_service_info($server->class);
      $classes = class_parents($class_info['class']);
      $classes[$class_info['class']] = $class_info['class'];
      if (!in_array('SearchApiSolrService', $classes)) {
        throw new SearchApiException(t('Server %server is not a Solr server, unable to extract file.', array(
          '%server' => $server_name,
        )));
      }

      // Open a connection to the server.
      $solr_connection = $server->getSolrConnection();

      // Path for our servlet request.
      $servlet_path = variable_get('search_api_attachments_extracting_servlet_path', 'update/extract');

      // Parameters for the extraction request.
      $params = array(
        'extractOnly' => 'true',
        'extractFormat' => 'text',
        'wt' => 'json',
        'hl' => 'on',
        'stream.url' => $url,
      );

      $options = array('method' => 'GET');

      // Make a servlet request using the solr connection.
      $response = $solr_connection->makeServletRequest($servlet_path, $params, $options);

      // If we have an extracted response, all is well.
      if (isset($response->{$url})) {
        $extraction = $response->{$url};
      }
    }
    catch (Exception $e) {
      // Log the exception to watchdog. Exceptions from Solr may be transient,
      // or indicate a problem with a specific file.
      watchdog('Search API Attachments Field Link', 'Exception occurred sending %url to Solr.', array('%url' => $url));
      watchdog_exception('Search API Attachments Field Link', $e);
    }

    return $extraction;
  }

  /**
   * {@inheritdoc}
   */
  public function propertyInfo() {
    $ret = array();
    if ($this->index->item_type != 'file') {
      $fields = $this->getLinkFields();
      foreach ($fields as $name => $field) {
        $ret['attachments_' . $name] = array(
          'label' => 'Attachment content: ' . $name,
          'description' => $name,
          'type' => 'text',
        );
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLinkFields() {
    $ret = array();
    foreach (field_info_fields() as $name => $field) {
      if ($field['type'] == 'link_field') {
        $ret[$name] = $field;
      }
    }
    return $ret;
  }
}
